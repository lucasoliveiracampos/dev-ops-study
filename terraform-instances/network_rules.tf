resource "aws_security_group" "sg_main" {
    name = "sg_main"
    description = "Allow TLS inbound traffic"
}

data "http" "myip" {
  url = "https://ifconfig.co/json"
  request_headers = {
    Accept = "application/json"
  }
}

locals {
  ifconfig_co_json = jsondecode(data.http.myip.response_body)
}

resource "aws_security_group_rule" "ingress_rabbitmq" {
    type = "ingress"
    from_port = 15672
    to_port = 15672
    protocol = "tcp"
    cidr_blocks = [
        "${local.ifconfig_co_json.ip}/32"
    ]
    security_group_id = aws_security_group.sg_main.id
}

resource "aws_security_group_rule" "ingress_ssh" {
    type = "ingress"
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = [
        "${local.ifconfig_co_json.ip}/32"
    ]
    security_group_id = aws_security_group.sg_main.id
}

resource "aws_security_group_rule" "ingress_http" {
    type = "ingress"
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = [
        "${local.ifconfig_co_json.ip}/32"
    ]
    security_group_id = aws_security_group.sg_main.id
}

resource "aws_security_group_rule" "ingress_https" {
    type = "ingress"
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = [
        "${local.ifconfig_co_json.ip}/32"
    ]
    security_group_id = aws_security_group.sg_main.id
}

resource "aws_security_group_rule" "egress_default" {
    type = "egress"
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
    security_group_id = aws_security_group.sg_main.id
}