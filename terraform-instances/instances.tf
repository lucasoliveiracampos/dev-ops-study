resource "aws_key_pair" "terraform_key" {
  key_name   = "terraform-key"
  public_key = var.aws_terraform_public_key
}

data "template_file" "script" {
  template = file("./scripts/app_server_init.tpl")

  vars = {
    puller_access_key = var.puller_access_key
    puller_secret_access_key = var.puller_secret_access_key
    region = var.region
    account_number = var.account_number
  }
}

resource "aws_instance" "app_server" {
  ami             = "ami-06e85d4c3149db26a"
  instance_type   = "t2.micro"

  associate_public_ip_address = true

  vpc_security_group_ids = [
    aws_security_group.sg_main.id
  ]

  key_name = "terraform-key"

  depends_on = [
    aws_key_pair.terraform_key
  ]

  user_data = data.template_file.script.rendered

  tags = {
    Name = var.instance_name
  }
}