output "instance_id" {
  description = "ID of the EC2 instance"
  value       = aws_instance.app_server.id
}

output "instance_public_ip" {
  description = "Public IP address of the EC2 instance"
  value       = aws_instance.app_server.public_ip
}

output "instance_public_dns" {
  description = "Public DSN address of the EC2 instance"
  value       = aws_instance.app_server.public_dns
}

# output "secret" {
#   value = aws_iam_access_key.ci_cd_acess_key.encrypted_secret
# }