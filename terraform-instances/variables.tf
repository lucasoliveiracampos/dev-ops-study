variable "instance_name" {
  type        = string
  default     = "server_aplication"
  description = "Value of the Name tag for the EC2 instance"
}

variable "instance_type" {
  type    = string
  default = "t2.micro"
}

variable "region" {
  type = string
  default = "us-west-2"
}

variable "account_number" {
  type = string
}

variable "aws_terraform_public_key" {
  type = string
}

variable "puller_access_key" {
  type = string
}

variable "puller_secret_access_key" {
  type = string
}