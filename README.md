# DEVOPS LEARNING PLAN

## OVERVIEW
The goal for this project is to be able to build an IaC (Infrastructure as Code) in the multiple cloud providers with all possible best practices and when one provider be unavailable the program must to switch to the other provider automatically. Also, it must to synchronize all information between one provider to another in case of difference of data between data.


## PROJECT
A producer and consumer using a message broker and based on some metrics (TO BE DEFINED) the result must be saved into a database for further analyses.


## SETUP
For this project work as expected follow the steps bellow:
1. Crete an account in *AWS*;
1. Install `awscli` tool;
1. Setup your AWS user in your terminal;
1. Install terraform;
1. Create a gpg key running command some command as `gpg --quick-gen-key gitlab_ci_cd`;
1. Set an environment variable `TF_VAR_ci_cd_credentials` with the gpg location (eg: `TF_VAR_ci_cd_credentials=./gitlab_ci_cd.gpg`);


## TASKS
- [ ] Create an infrastructure as code using TERRAFORM
  - [ ] Create three kind of users (Super User, DBA, Autoscaling User, Accounting)
  - [ ] IAM (Identity and Access Management);
  - [ ] Must to use VPC and Subnet;
  - [ ] Must to scale automatically - Using Auto Scaling Group;
  - [ ] Must to have environments: DEV, QA, PROD;
  - [ ] Fully tested;
  - [ ] Change to another provider when the first one is not available; 
    - [ ] Must to do all healthy checks before forward the traffic to the new provider to avoid any issue;
  - [ ] Create a BASTION;
- [ ] Use some tool to store sensitive data. (e.g: [Vault](https://developer.hashicorp.com/vault), [aws secrets manager](https://aws.amazon.com/secrets-manager/), [Azure key vault](https://azure.microsoft.com/en-us/products/key-vault/#product-overview));
- [ ] Create a pipeline with continuous integration and software image rotation (for rollbacks, for example);
- [ ] Configure machines as needed using ANSIBLE;
- [ ] Be linked with Jenkins based on new commits;
  - [ ] Must to trigger tests and commit into BETA when all tests are green;
  - [ ] Add baking test and if it pass, merge automatically into MASTER;

## AWS Notes
Log from AWS EC2 "user_data" (init scripts) are located on `/var/log/cloud-init-output.log` (helpful for debugging).


## Current thoughts about how to architect it

I'm thinking to use Terraform workspace for infrastructure's automated testing. For example, the application create some ec2 instances with a name as bellow:
```bash
resource "aws_instance" "example" {
  tags = {
    Name = "web - ${terraform.workspace}"
  }

  # ... other arguments
}
```

## Cautions
- Sensitive data must be stored in a *file* or *environment variables* and NOT hardcoded; 
- If used `time credentials` they may expire before finishing the plan (execution?) (see details [here](https://developer.hashicorp.com/terraform/language/settings/backends/configuration#using-a-backend-block));
- Before migrating to a new backend, it's strongly recommend manually backing up your state by copying your `terraform.tfstate` file to another location. ([source](https://developer.hashicorp.com/terraform/language/settings/backends/configuration#using-a-backend-block));
 

## BEST PRACTICES
- Terraform usually automatically determines which `provider` to use based on a resource type's name. (By convention, resource type names start with their provider's preferred local name.) When using multiple configurations of a provider (or non-preferred local provider names), you must use the provider meta-argument to manually choose an alternate provider configuration. See the [provider meta-argument](https://developer.hashicorp.com/terraform/language/meta-arguments/resource-provider) for more details;


## To Study
- terraform-expressions: https://developer.hashicorp.com/terraform/language/expressions